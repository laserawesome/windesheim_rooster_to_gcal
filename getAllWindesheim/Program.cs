﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.SharePoint.Client;
using System.Security;
using RestSharp;
using Microsoft.SharePoint.Client.Search.Query;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using static Google.Apis.Calendar.v3.CalendarListResource;
using System.Text.RegularExpressions;

namespace getAllWindesheim
{


    class Program
    {
        private static readonly string filelocation = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Windesheimrooster\rooster_classen.json";
        private static readonly string filemap = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Windesheimrooster\";
        private static readonly string filelocation_2 = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Windesheimrooster\rooster_volledig.json";
        private static readonly string credpathlocation = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)  + @"\Windesheimrooster\token.json";
        private static string[] Scopes = { CalendarService.Scope.CalendarEvents, CalendarService.Scope.CalendarReadonly };
        private static bool arghandler = false;
        private static string ApplicationName = "windesheimrooster";
        private static bool Update = false;
        private static string[] GlobalArgs { get; set; }


        static RestClient client = new RestClient("http://api.windesheim.nl/api/");

        static void Main(string[] args)
        {
            Arghanlder(args);

            bool loop = true;



            while (loop)
            {

                Console.WriteLine("Klas(1) of Vak(2) of Update(3) ?: ");
                string x = null;
                if (arghandler)
                {
                    x = GlobalArgs[0];
                }
                else
                {
                   x = Console.ReadLine();
                }

                if (x == "1")
                {
                    loop = false;
                    Console.WriteLine("Geef klas code: ");
                    string course_id = null;
                    if (arghandler)
                    {
                        course_id = GlobalArgs[1];
                    }
                    else
                    {
                        course_id = Console.ReadLine();
                    }
                    doesKlasorVakExist(course_id, true);
                    KlasofVak klas = doesKlasorVakExist(course_id, true);
                    if (klas.DoesKlasorVakexist)
                    {
                        GoogleCalendarPush(klas.Klas);
                        Console.WriteLine("Gelukt!");
                        Console.WriteLine("Druk op willekurige knop op programma te beëindigen");
                        Console.ReadKey();
                    }
                    else
                    {

                        bool check = true;
                        while (check)
                        {
                            Console.WriteLine("Klas: " + course_id + "is niet gevonden of bestand bestaat niet. Wil je een zoekpoging doen?(J/N)");
                            string question = Console.ReadLine();
                            if (question == "J")
                            {
                                RoosterRequest();
                                klas = doesKlasorVakExist(course_id, true);
                                if (klas.DoesKlasorVakexist)
                                {
                                    GoogleCalendarPush(klas.Klas);
                                    Console.WriteLine("Gelukt!");
                                    Console.WriteLine("Druk op willekurige knop op programma te beëindigen");
                                    Console.ReadKey();
                                }
                                else
                                {
                                    Console.WriteLine("Klas: " + course_id + " is niet gevonden");
                                }
                                check = false;
                            }
                            else if (question == "N")
                            {
                                check = false;
                            }
                            else
                            {
                                Console.WriteLine("Ongelige invoer");
                            }
                        }

                    }

                }
                else if (x == "2")
                {
                    loop = false;
                    Console.WriteLine("Geef vak naam of code: ");
                    string vak_id = null;
                    if (arghandler)
                    {
                        vak_id = GlobalArgs[1];
                    }
                    else
                    {
                        vak_id = Console.ReadLine();
                    }

                    KlasofVak vak = doesKlasorVakExist(vak_id, false);
                    if (vak.DoesKlasorVakexist)
                    {
                        GoogleCalendarPush(vak.Vak);
                        Console.WriteLine("Gelukt!");
                        Console.WriteLine("Druk op willekurige knop op programma te beëindigen");
                        Console.ReadKey();
                    }
                    else
                    {

                        bool check = true;
                        while (check)
                        {
                            Console.WriteLine("Vak: " + vak_id + "is niet gevonden of bestand bestaat niet. Wil je een zoekpoging doen?(J/N)");
                            string question = Console.ReadLine();
                            if (question == "J")
                            {
                                RoosterRequest();
                                vak = doesKlasorVakExist(vak_id, false);
                                if (vak.DoesKlasorVakexist)
                                {
                                    GoogleCalendarPush(vak.Vak);
                                    Console.WriteLine("Gelukt!");
                                    Console.WriteLine("Druk op willekurige knop op programma te beëindigen");
                                    Console.ReadKey();
                                }
                                else
                                {
                                    Console.WriteLine("Vak: " + vak_id + " is niet gevonden");
                                }
                                check = false;
                            }
                            else if (question == "N")
                            {
                                check = false;
                            }
                            else
                            {
                                Console.WriteLine("Ongelige invoer");
                            }
                        }

                    }


                }
                else if (x == "3")
                {
                    Update = true;
                    RoosterRequest();
                }
                else
                {
                    Console.WriteLine("Ongelige invoer");
                }
            }







            Console.ReadLine();


        }

        private static void RoosterRequest()
        {




            if (!Directory.Exists(filemap))
            {
                Directory.CreateDirectory(filemap);
            }





            if (System.IO.File.Exists(filelocation) == true)
            {
                string text = System.IO.File.ReadAllText(filelocation);
                DataTable dataTable = JsonConvert.DeserializeObject<DataTable>(text);
                DataTable fetcheddataTable = null;
                string textfromfile = null;
                Console.WriteLine("Fetching Classes...");

                foreach (DataRow row in dataTable.Rows)
                {
                    if (System.IO.File.Exists(filelocation_2))
                    {
                        break;

                    }

                    else
                    {
                        var stringcorrector = row["id"] as string;
                        if (stringcorrector.Contains("&"))
                        {
                            continue;
                        }
                   var deeprequest = new RestRequest("Klas/{entity}/Les?$orderby=starttijd").AddParameter("entity",stringcorrector, ParameterType.UrlSegment);

                        var response = client.Get(deeprequest);
                        Console.WriteLine("Requested: " + response.ResponseUri);
                        Console.WriteLine("---------------------------------------------------------------------------------");
                        Console.WriteLine("Fetched: " + response.Content);
                        var fetch = response.Content;


                        if (fetcheddataTable == null)
                        {
                            fetcheddataTable = JsonConvert.DeserializeObject<DataTable>(fetch);
                        }
                        else
                        {
                            fetcheddataTable.Merge(JsonConvert.DeserializeObject<DataTable>(fetch));
                        }
                    }
                }

                textfromfile = JsonConvert.SerializeObject(fetcheddataTable, Formatting.Indented);

                System.IO.File.WriteAllText(filelocation_2, textfromfile);

            }
            else if(Update == true || System.IO.File.Exists(filelocation) == false)
            {








                try
                {

                   

                    var request = new RestRequest("Klas/");
                    Console.WriteLine("Fetching Classes...");
                    var response = client.Get(request);


                    System.IO.File.WriteAllText(filelocation, response.Content);
                    string text = System.IO.File.ReadAllText(filelocation);
                    DataTable dataTable = JsonConvert.DeserializeObject<DataTable>(text);
                    DataTable fetcheddataTable = null;

                    Console.WriteLine("Fetching Lessen...");

                    foreach (DataRow row in dataTable.Rows)
                    {
                        var stringcorrector = row["id"] as string;
                        if (stringcorrector.Contains("&"))
                        {
                            continue;
                        }


                        var deeprequest = new RestRequest("Klas/{entity}/Les?$orderby=starttijd").AddParameter("entity", stringcorrector, ParameterType.UrlSegment);
                        response = client.Get(request);
                        var fetch = response.Content;
                        Console.WriteLine("Requested: " + response.Request);
                        Console.WriteLine("---------------------------------------------------------------------------------");
                        Console.WriteLine("Fetched: " + response.Content);

                        if (System.IO.File.Exists(filelocation_2) == true)
                        {

                            fetcheddataTable = JsonConvert.DeserializeObject<DataTable>(fetch);
                            string storedtext = System.IO.File.ReadAllText(filelocation);
                            DataTable storeddataTable = JsonConvert.DeserializeObject<DataTable>(storedtext);
                            foreach (DataRow row2 in storeddataTable.Rows)
                            {
                                foreach (DataRow row3 in fetcheddataTable.Rows)
                                {
                                    if (row2 == row3)
                                    {
                                        continue;
                                    }
                                    else
                                    {
                                        if (row2["id"] == row3["id"])
                                        {
                                            storeddataTable = storechanges(storeddataTable, row3, row2);
                                        }
                                        else
                                        {
                                            storeddataTable = storechanges(storeddataTable, row3);
                                        }
                                    }
                                }

                            }
                            
                            string json = JsonConvert.SerializeObject(dataTable, Formatting.Indented);
                            System.IO.File.WriteAllText(filelocation_2, json);


                        }
                        else
                        {
                            if (fetcheddataTable == null)
                            {
                                fetcheddataTable = JsonConvert.DeserializeObject<DataTable>(fetch);
                            }
                            else
                            {
                                fetcheddataTable.Merge(JsonConvert.DeserializeObject<DataTable>(fetch));
                            }
                        }
                    }






                }
                catch (Exception e)
                {
                    Console.WriteLine("\nException Caught!");
                    Console.WriteLine("Message: {0}", e.Message);
                    string x = Console.ReadLine();
                }
                {

                }



            }


        }
        private static DataTable storechanges(DataTable original, DataRow newer, DataRow old)
        {
            original.Rows.Remove(old);
            original.Rows.Add(newer);

            return original;
        }


        private static DataTable storechanges(DataTable original, DataRow newer)
        {
            original.Rows.Add(newer);

            return original;
        }

        private static KlasofVak doesKlasorVakExist(string klasofVak, bool isKlas)
        {
            if (System.IO.File.Exists(filelocation) == false)
            {
                return new KlasofVak(false);
            }
            else if (System.IO.File.Exists(filelocation_2) == false)
            {
                return new KlasofVak(false);
            }
            else
            {
                string text = System.IO.File.ReadAllText(filelocation_2);
                var fetcheddataTable = JsonConvert.DeserializeObject<DataTable>(text);
                DataRow[] foundRows = null;

                if (isKlas)
                {
                    foundRows = fetcheddataTable.Select("groepcode = '" + klasofVak + "'");
                    if (foundRows == null || foundRows.Length == 0)
                    {
                        return new KlasofVak(false);
                    }
                    else
                    {
                        return new KlasofVak(true, foundRows, isKlas);
                    }

                }
                else
                {
                    foundRows = fetcheddataTable.Select("vakcode = '" + klasofVak + "'");
                    if (foundRows == null || foundRows.Length == 0)
                    {
                        return new KlasofVak(false);
                    }
                    else
                    {
                        return new KlasofVak(true, foundRows, isKlas);
                    }

                }



            }
        }

        private static void GoogleCalendarPush(DataRow[] rows)
        {

            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = credpathlocation;
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }
            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });


            ArrayList events = new ArrayList();
            DateTime dateTimemax = DateTime.Now.AddMonths(3);

            foreach (DataRow row in rows)
            {

                if (DateTimeOffset.FromUnixTimeMilliseconds((long)row["starttijd"]).UtcDateTime.AddHours(-2) > dateTimemax || row["vaknaam"] as string == "")
                {
                    continue;
                }
                else
                {
                    Event newEvent = new Event()
                    {
                        Summary = row["vaknaam"] as string,
                        Location = row["lokaal"] as string,
                        Description = row["commentaar"] as string + "\n Docent(en): " + (string.Join(", ", (string[])row["docentnamen"]) + "\n\n id = " + (string)row["id"]),
                        Start = new EventDateTime()
                        {
                            DateTime = DateTimeOffset.FromUnixTimeMilliseconds((long)row["starttijd"]).UtcDateTime.AddHours(-2),
                            TimeZone = "Europe/Amsterdam",
                        },
                        End = new EventDateTime()
                        {
                            DateTime = DateTimeOffset.FromUnixTimeMilliseconds((long)row["eindtijd"]).UtcDateTime.AddHours(-2),
                            TimeZone = "Europe/Amsterdam",
                        },
                        Reminders = new Event.RemindersData()
                        {
                            UseDefault = true
                        }



                    };
                    events.Add(newEvent);
                }
                
            }

            bool exists = false;
            Calendarinfo selectedone = null;


            while (!exists)
            {
                string pagetoken = null;
                ArrayList calendars = new ArrayList();
                do
                {

                    ListRequest calendarListentries = service.CalendarList.List();
                    calendarListentries.PageToken = pagetoken;
                    CalendarList calendarList = calendarListentries.Execute();
                    var calendaritems = calendarList.Items;
                    foreach (CalendarListEntry item in calendaritems)
                    {
                        calendars.Add(new Calendarinfo(item.Id, item.Summary));
                    }


                } while (pagetoken != null);

                Console.WriteLine("Voer agenda in waarin je alles wilt toevoegen: ");
                string inputcalendar = null;
                if (arghandler)
                {
                    inputcalendar = GlobalArgs[3];
                }
                else
                {
                    inputcalendar = Console.ReadLine();
                }
                


                for (int x = 0; x < calendars.Count; x++)
                {
                    if (((Calendarinfo)calendars[x]).Summary == inputcalendar)
                    {
                        exists = true;
                        selectedone = ((Calendarinfo)calendars[x]);

                    }
                }
                if (!exists)
                {
                    Console.WriteLine("Bestaat niet, probeer opniew.");
                }
            }
            ArrayList eventarraylist = new ArrayList();
            string eventpagetoken = null;
            do
            {
                var eventsfromcalendar = service.Events.List(selectedone.CalendarId);
                eventsfromcalendar.PageToken = eventpagetoken;
                var eventlists = eventsfromcalendar.Execute();

                var eventitems = eventlists.Items;
                foreach (Event eventz in eventitems)
                {
                    eventarraylist.Add(eventz);
                }
            } while (eventpagetoken != null);

            for (int x = 0; x < eventarraylist.Count; x++)
            {
                for (int y = 0; y < events.Count; y++)
                {
                    string[] stringSeparators = new string[] { "id = " };
                    string[] id_calendar = null;
                    string[] id_local = null;

                    if (x == 0 && y == 0)
                    {
                        id_local = ((string)((Event)events[y]).Description).Split(stringSeparators, StringSplitOptions.None);
                        id_calendar = ((string)((Event)eventarraylist[x]).Description).Split(stringSeparators, StringSplitOptions.None);
                    }
                    else if (x == 0)
                    {
                        id_local = ((string)((Event)events[(y)]).Description).Split(stringSeparators, StringSplitOptions.None);
                        id_calendar = ((string)((Event)eventarraylist[x]).Description).Split(stringSeparators, StringSplitOptions.None);
                    }
                   
                    else if(y == 0)
                    {
                        id_local = ((string)((Event)events[(y)]).Description).Split(stringSeparators, StringSplitOptions.None);
                        id_calendar = ((string)((Event)eventarraylist[x]).Description).Split(stringSeparators, StringSplitOptions.None);

                    }
                    else
                    {
                        id_local = ((string)((Event)events[(y- 1)]).Description).Split(stringSeparators, StringSplitOptions.None);
                        id_calendar = ((string)((Event)eventarraylist[(x - 1)]).Description).Split(stringSeparators, StringSplitOptions.None);
                    }

                    if (id_calendar[1] == id_local[1])
                    {
                        EventsResource.UpdateRequest request = service.Events.Update( ((Event)events[y]), selectedone.CalendarId, ((Event)eventarraylist[x]).Id );
                        if (y == 0)
                        {
                            events.Remove(events[y]);
                        }
                        else
                        {
                            events.Remove(events[y-1]);
                        }
                    }
                    else if ((((Event)eventarraylist[x]).Summary == ((Event)events[y]).Summary) && (((Event)eventarraylist[x]).Description == ((Event)events[y]).Description) && (((Event)eventarraylist[x]).Start == ((Event)events[y]).Start) && (((Event)eventarraylist[x]).End == ((Event)events[y]).End))
                    {
                        if (x == 0)
                        {
                            events.Remove(events[x]);
                        }
                        else
                        {
                            events.Remove(events[x - 1]);
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }


            Console.WriteLine("Log: ");
            for (int f = 0; f < events.Count; f++)
            {
                EventsResource.InsertRequest request = service.Events.Insert(((Event)events[f]), selectedone.CalendarId);
                Event createdEvent = request.Execute();

                Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);
                


            }
            return;

        }

        private static void Arghanlder(string [] args)
        {
            if(args.Length <= 0)
            {
                return;
            }
            else
            {
                ArrayList argbuilder = new ArrayList();
                bool nextinput = false;
                int pos = Array.IndexOf(args, "-c");
                
                if(pos > -1 )
                {
                    argbuilder.Add(args[pos + 1]);
                    pos = Array.IndexOf(args, "-k");
                    if (pos > -1)
                    {
                        argbuilder.Add(args[pos + 1]);
                        if (args[pos + 1] == "3")
                        {
                            arghandler = true;
                            return;
                        }
                    }
                    else
                    {
                        pos = Array.IndexOf(args, "-v");
                        if (pos > -1)
                        {
                            argbuilder.Add(args[pos + 1]);
                            pos = Array.IndexOf(args, "-g");
                            if (pos > -1)
                            {
                                argbuilder.Add(args[pos + 1]);
                                arghandler = true;
                                GlobalArgs = ((string [])argbuilder.ToArray());
                                return;
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    return;
                }
                

            }
        }

}
}
