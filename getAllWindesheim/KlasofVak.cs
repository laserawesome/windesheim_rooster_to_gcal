﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getAllWindesheim
{
    class KlasofVak
    {
        public bool DoesKlasorVakexist { get; set; }
        public DataRow[] Klas { get; set; }
        public DataRow[] Vak { get; set; }



        public KlasofVak(bool doesKlasorVakexist)
        {
            DoesKlasorVakexist = doesKlasorVakexist;
        }

        public KlasofVak(bool doesKlasorVakexist, DataRow [] klasofvak, bool isKlas)
        {
            DoesKlasorVakexist = doesKlasorVakexist;
            if (isKlas)
            {
                Klas = klasofvak;
            }
            else
            {
                Vak = klasofvak;

            }
        }

    }
}
